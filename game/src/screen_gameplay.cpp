/**********************************************************************************************
*
*   raylib - Advance Game template
*
*   Gameplay Screen Functions Definitions (Init, Update, Draw, Unload)
*
*   Copyright (c) 2014-2022 Ramon Santamaria (@raysan5)
*
*   This software is provided "as-is", without any express or implied warranty. In no event
*   will the authors be held liable for any damages arising from the use of this software.
*
*   Permission is granted to anyone to use this software for any purpose, including commercial
*   applications, and to alter it and redistribute it freely, subject to the following restrictions:
*
*     1. The origin of this software must not be misrepresented; you must not claim that you
*     wrote the original software. If you use this software in a product, an acknowledgment
*     in the product documentation would be appreciated but is not required.
*
*     2. Altered source versions must be plainly marked as such, and must not be misrepresented
*     as being the original software.
*
*     3. This notice may not be removed or altered from any source distribution.
*
**********************************************************************************************/

#include <cmath>
#include <list>
#include <string>

#include "raylib.h"
#include "screens.h"

struct AnimData
{
    int currentFrame{};
    float SpriteUpdateTime = 0.1f;
    float ElapsedTimeSinceSpriteUpdate{};
};

struct ObjectData
{
    Texture2D texture{};
    Rectangle SpriteRec = {0, 0, 0, 0};
    Rectangle CollisionRec = {0, 0, 0, 0};
    float colliderOffset = 0;
    Vector2 pos{};
    AnimData anim;
};

struct BackgroundData
{
    Texture2D texture{};
    std::list<float> XPos{};
    float scale{};
    int poolSize{};
};

//----------------------------------------------------------------------------------
// Module Variables Definition (local)
//----------------------------------------------------------------------------------
static int framesCounter = 0;
static int finishScreen = 0;


//===============Misc===============

//Acceleration due to gravity (pixels/s)/s
constexpr float gravity = 5000;

float groundPosition;
float deltaTime;

BackgroundData background;
BackgroundData midground;
BackgroundData foreground;

int score = 0;

//===============Character===============

ObjectData character;
//pixels/s
const int jumpVelocity = -1500;
float characterYVelocity;

//===============Obstacle===============

//pixels/s
float ObstacleXVelocity = -600;
constexpr int sizeOfObstacles = 10;
ObjectData obstacles[sizeOfObstacles];

//----------------------------------------------------------------------------------
// Gameplay Screen Functions Definition
//----------------------------------------------------------------------------------
float getGroundPositionForObject(float objectHeight)
{
    return  static_cast<float>(GetScreenHeight()) - objectHeight;
}

// Gameplay Screen Initialization logic
void InitGameplayScreen(void)
{
    score=0;
    framesCounter = 0;
    finishScreen = 0;

    background.texture = LoadTexture("resources/textures/background.png");
    background.scale = static_cast<float>(GetScreenHeight()) / static_cast<float>(background.texture.height);
    background.poolSize = static_cast<int>(ceil( static_cast<float>(GetScreenWidth())
        /  static_cast<float>(background.texture.width)*background.scale)) +1;
    for(int i = 0; i <background.poolSize; i++) background.XPos.push_back
        (( static_cast<float>(background.texture.width))*background.scale*static_cast<float>(i));

    midground.texture = LoadTexture("resources/textures/midground.png");
    midground.scale = static_cast<float>(GetScreenHeight()) / static_cast<float>(midground.texture.height);
    midground.poolSize = static_cast<int>(ceil( static_cast<float>(GetScreenWidth())
        /  static_cast<float>(midground.texture.width)*midground.scale)) +1;
    for(int i = 0; i <midground.poolSize; i++) midground.XPos.push_back
        (( static_cast<float>(midground.texture.width))*midground.scale*static_cast<float>(i));

    foreground.texture = LoadTexture("resources/textures/foreground.png");
    foreground.scale = static_cast<float>(GetScreenHeight()) / static_cast<float>(foreground.texture.height);
    foreground.poolSize = static_cast<int>(ceil( static_cast<float>(GetScreenWidth())
        /  static_cast<float>(foreground.texture.width)*foreground.scale)) +1;
    for(int i = 0; i <foreground.poolSize; i++) foreground.XPos.push_back
        (( static_cast<float>(foreground.texture.width))*foreground.scale*static_cast<float>(i));

   
    character.texture = LoadTexture("resources/textures/scarfy.png");
    character.pos = {static_cast<float>(GetScreenWidth()) / 2 - character.SpriteRec.width / 2, 0};
    character.SpriteRec.width = static_cast<float>(character.texture.width) / 6;
    character.SpriteRec.height = static_cast<float>(character.texture.height);
    character.pos.y = getGroundPositionForObject(character.SpriteRec.height);

    auto posX = static_cast<float>(GetScreenWidth());
    for (auto& obstacle : obstacles)
    {
        obstacle.texture = LoadTexture("resources/textures/nebula.png");
        obstacle.pos = {posX , 0};
        obstacle.SpriteRec.width = static_cast<float>(obstacle.texture.width) / 8;
        obstacle.SpriteRec.height = static_cast<float>(obstacle.texture.height) / 8;
        obstacle.colliderOffset = 50;
        obstacle.pos.y = getGroundPositionForObject(obstacle.SpriteRec.height);

        posX += static_cast<float>(GetRandomValue(600, 1000));
    }
}

bool IsGrounded()
{
    if (character.pos.y >= getGroundPositionForObject(character.SpriteRec.height)) return true;
    return false;
}

void ApplyGravity()
{
    if (IsGrounded())
    {
        characterYVelocity = 0;
    }
    else characterYVelocity += gravity * deltaTime;
}

void CharacterJump()
{
    if (!IsGrounded()) return;

    if (IsKeyPressed(KEY_SPACE) || IsGestureDetected(GESTURE_TAP))
    {
        characterYVelocity += jumpVelocity;
        PlaySound(fxCoin);
    }
}

void UpdatePosition(Vector2& position, float xVelocity, float yVelocity)
{
    position.x += static_cast<float>(xVelocity) * deltaTime;
    position.y += static_cast<float>(yVelocity) * deltaTime;
}

void UpdateCollisionRec(ObjectData &object)
{
    object.CollisionRec = {
    object.pos.x + object.colliderOffset,
    object.pos.y + object.colliderOffset,
    object.SpriteRec.width -2*object.colliderOffset,
    object.SpriteRec.height -2*object.colliderOffset
    };
}

// Gameplay Screen Update logic
void UpdateGameplayScreen(void)
{
    deltaTime = GetFrameTime();
    score += static_cast<int>(100 * deltaTime);
    ApplyGravity();

    CharacterJump();

    UpdatePosition(character.pos, 0, characterYVelocity);

    for (auto& obstacle : obstacles)
    {
        UpdatePosition(obstacle.pos, ObstacleXVelocity, 0);
    }

    UpdateCollisionRec(character);

    for (auto obstacle : obstacles)
    {
        UpdateCollisionRec(obstacle);
        
        if(CheckCollisionRecs(obstacle.CollisionRec, character.CollisionRec))
        {
            finishScreen = 1;
            PlaySound(fxCoin);
        }
    }
    
}

void UpdateAnimationFrame(float& AnimElapsedTimeSinceLastFrame, float AnimFrameUpdateTime, Rectangle& rectangle,
                          int& currentFrame, int maxFrame)
{
    AnimElapsedTimeSinceLastFrame += deltaTime;

    if (AnimElapsedTimeSinceLastFrame >= AnimFrameUpdateTime)
    {
        AnimElapsedTimeSinceLastFrame = 0;
        rectangle.x = static_cast<float>(currentFrame) * rectangle.width;
        currentFrame++;
        if (currentFrame > maxFrame) currentFrame = 0;
    }
}

void DrawBackGround(BackgroundData &backgroundData, float speed)
{
    float i =0;
    for (auto& pos : backgroundData.XPos)
    {
        pos -= speed * deltaTime;
        if(pos <= static_cast<float>( -backgroundData.texture.width) * backgroundData.scale){
            pos = (static_cast<float>(backgroundData.XPos.size()) - 1) * ( static_cast<float>(backgroundData.texture.width) * backgroundData.scale) - speed * deltaTime;
        }
        
        Vector2 bgPos{pos , 0.0};
        DrawTextureEx(backgroundData.texture, bgPos, 0.0, backgroundData.scale, WHITE);
        i++;
    }
}

// Gameplay Screen Draw logic
void DrawGameplayScreen(void)
{
    DrawBackGround(background, 100);
    DrawBackGround(midground, 200);
    DrawBackGround(foreground, 400);

    DrawText(std::to_string(score).c_str(), GetScreenWidth()/10, GetScreenHeight()/10 , 80, RED);
    
    //Draw obstacle
    for (const auto& obstacle : obstacles)
    {
        DrawTextureRec(obstacle.texture, obstacle.SpriteRec, obstacle.pos, WHITE);
    }

    //Draw character
    DrawTextureRec(character.texture, character.SpriteRec, character.pos, WHITE);

    if (IsGrounded())
        UpdateAnimationFrame(character.anim.ElapsedTimeSinceSpriteUpdate,
                             0.1f, character.SpriteRec, character.anim.currentFrame, 5);

    for (auto& obstacle : obstacles)
    {
        UpdateAnimationFrame(obstacle.anim.ElapsedTimeSinceSpriteUpdate, 0.1f,
                             obstacle.SpriteRec, obstacle.anim.currentFrame, 7);

        obstacle.anim.ElapsedTimeSinceSpriteUpdate += deltaTime;
    }
}

// Gameplay Screen Unload logic
void UnloadGameplayScreen(void)
{
    // TODO: Unload GAMEPLAY screen variables here!
    UnloadTexture((character.texture));

    for (const auto& obstacle : obstacles)
    {
        UnloadTexture(obstacle.texture);
    }

    UnloadTexture(background.texture);
}

// Gameplay Screen should finish?
int FinishGameplayScreen(void)
{
    return finishScreen;
}
